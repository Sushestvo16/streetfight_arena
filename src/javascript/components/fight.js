import { controls } from '../../constants/controls';
import { showWinnerModal } from '../components/modal/winner'
export async function fight(firstFighter, secondFighter) {

  let hp_leftFighter = document.getElementById('left-fighter-indicator');
  let hp_rightFighter = document.getElementById('right-fighter-indicator');
  firstFighter.dynamicHealth = firstFighter.health;
  secondFighter.dynamicHealth = secondFighter.health;
  hp_leftFighter.innerHTML = `${firstFighter.health}hp`;
  hp_rightFighter.innerHTML = `${secondFighter.health}hp`;
  firstFighter.combo = [];
  secondFighter.combo = [];
  firstFighter.allowCombo = true;
  secondFighter.allowCombo = true;
  firstFighter.firstFighterWin = false;
  secondFighter.secondFighterWin = false;




  document.addEventListener('keydown', (e) => {

    for (const [key, value] of Object.entries(controls)) {
      firstFighter.isCombo = false;
      secondFighter.isCombo = false;
      const keyName = e.code;

      if (Array.isArray(value)) {
        //загоняем кнопку в массив, если она есть в комбинации
        if (key == 'PlayerOneCriticalHitCombination' && controls['PlayerOneCriticalHitCombination'].includes(keyName)) {
          firstFighter.combo.push(keyName);

        }
        if (key == 'PlayerTwoCriticalHitCombination' && controls['PlayerTwoCriticalHitCombination'].includes(keyName)) {
          secondFighter.combo.push(keyName);

        }

        firstFighter.isCombo = JSON.stringify(firstFighter.combo.sort()) == JSON.stringify(value.sort()) && firstFighter.allowCombo;


        secondFighter.isCombo = JSON.stringify(secondFighter.combo.sort()) == JSON.stringify(value.sort()) && secondFighter.allowCombo;


      }


      if (keyName == value || firstFighter.isCombo || secondFighter.isCombo) {
        let damage = 0;

        function Attack(attacker, defender, hp_defender) {
          // if (attacker.isDefending == true);
          // attacker.isAttacking = true;
          damage = getDamage(attacker, defender);

          defender.dynamicHealth -= damage;
          hp_defender.innerHTML = `${defender.dynamicHealth}hp`;
          if (defender.dynamicHealth <= 0) {
            Winner(attacker);
            defender.dynamicHealth = 0;
            hp_defender.innerHTML = `0hp`
          }
          return hp_defender, damage;


        }
        function Deffend(attacker) {
          attacker.isDefending = true;
          damage = getBlockPower(attacker);
        }


        switch (key) {

          case 'PlayerOneAttack':
            if (firstFighter.isDefending == true) break;
            firstFighter.isAttacking = true;
            // damage = getDamage(firstFighter, secondFighter);
            // health_rightFighter -= damage;
            // hp_rightFighter.innerHTML = `${health_rightFighter}hp`;
            // if (health_rightFighter <= 0) {
            //   Winner(firstFighter);
            //   health_rightFighter = 0;
            //   hp_rightFighter.innerHTML = `0hp`
            // }
            Attack(firstFighter, secondFighter, hp_rightFighter)





            break;

          case 'PlayerOneBlock':
            if (firstFighter.isAttacking == true) break;
            if (secondFighter.isCombo) break;
            // firstFighter.isDefending = true;
            // damage = getBlockPower(firstFighter);
            Deffend(firstFighter)

            break;
          case 'PlayerTwoAttack':
            if (secondFighter.isDefending == true) break;
            secondFighter.isAttacking = true;
            // damage = getDamage(secondFighter, firstFighter);
            // health_leftFighter -= damage;
            // hp_leftFighter.innerHTML = `${health_leftFighter}hp`;
            // if (health_leftFighter <= 0) {

            //   Winner(secondFighter);
            //   health_leftFighter = 0;
            //   hp_leftFighter.innerHTML = `0hp`;

            // }
            Attack(secondFighter, firstFighter, hp_leftFighter)
            break;
          case 'PlayerTwoBlock':
            if (secondFighter.isAttacking == true) break;
            if (firstFighter.isCombo) break;

            Deffend(secondFighter)
            break;
          case 'PlayerOneCriticalHitCombination':
            firstFighter.allowCombo = false;
            damage = getDamage(firstFighter, secondFighter);
            setTimeout(() => {
              firstFighter.allowCombo = true;
              console.log('comboo firstFighter now true')
            }, 10000)
            Attack(firstFighter, secondFighter, hp_rightFighter)


            break;
          case 'PlayerTwoCriticalHitCombination':
            secondFighter.allowCombo = false;

            setTimeout(() => {
              secondFighter.allowCombo = true;
              console.log('comboo secondFighter now true')
            }, 10000)
            Attack(secondFighter, firstFighter, hp_leftFighter)

            break;
          default:
            console.log('No');
            break;

        }
      }



    }

  })
  document.addEventListener('keyup', (event) => {
    const keyName = event.code;
    if (controls['PlayerOneCriticalHitCombination'].includes(keyName)) {
      firstFighter.combo = firstFighter.combo.filter(keyCode => keyCode != keyName)


    }

    if (controls['PlayerTwoCriticalHitCombination'].includes(keyName)) {
      secondFighter.combo = secondFighter.combo.filter(keyCode => keyCode != keyName)
    }




    for (const [key, value] of Object.entries(controls)) {

      if (keyName == value) {
        switch (key) {
          case 'PlayerOneAttack':
            firstFighter.isAttacking = false;

            break;
          case 'PlayerOneBlock':
            firstFighter.isDefending = false;
            break;
          case 'PlayerTwoAttack':
            secondFighter.isAttacking = false;
            break;
          case 'PlayerTwoBlock':
            secondFighter.isDefending = false;
            break;
          default:
            alert('Неизвестное значение');
        }
      }
    }
  })
  const Winner = (winner) => {
    return new Promise((resolve) => {
      // resolve the promise with the winner when fight is over
      if (winner == secondFighter) {
        secondFighter.secondFighterWin = true;
      } else if (winner == firstFighter) {
        firstFighter.firstFighterWin = true;
      }
      if (winner) {
        resolve(showWinnerModal(winner))
      } else {
        reject(console.log("Error."))
      }
    });
  }

}

export function getDamage(attacker, defender) {
  // return damages

  if (defender.health <= 0) {
    Winner(attacker);
    defender.health = 0;
  }
  let damage = getHitPower(attacker) - (defender.isDefending && !attacker.isCombo ? getBlockPower(defender) : 0);
  console.log(damage)

  console.log('- здоровье соперника:', damage);


  return damage > 0 ? damage : 0
}

export function getHitPower(fighter) {

  // return hit power
  const { attack } = fighter;
  let max = 2;
  const criticalHitChance = Math.ceil(Math.random() * (max))
  let hitPower;
  hitPower = attack * criticalHitChance;
  if (fighter.isCombo) {
    hitPower = attack * 2;
  }
  console.log(`урон от ${fighter.name}`, hitPower);
  return hitPower
}

export function getBlockPower(fighter) {
  // return block power
  const { defense } = fighter;
  let max = 2;
  let dodgeChance = Math.ceil(Math.random() * (max));
  let blockPower = defense * dodgeChance;
  console.log(`блок от ${fighter.name}`, blockPower);
  return blockPower;
}
