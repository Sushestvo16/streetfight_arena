import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {


  if (!fighter) return '';
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';

  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)

  const { name, source, attack, health, defense } = fighter;

  fighterElement.innerHTML = `<div class="title__preview">${name}</div>
    <img class=${positionClassName} src=${source} />
    <div class="preview__properties">
    <span >HEALTH:${health}</span>
    </div >
    <div class="preview__properties">
    <span>ATTACK:${attack}</span>
    </div>
    <div class="preview__properties">
    <span>DEFENSE:${defense}</span>
    </div>
    `


  return fighterElement
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
