import { showModal } from './modal'

export function showWinnerModal(fighter) {
  // call showModal function 
  let bodyElement = '';
  const { name } = fighter;
  if (fighter.leftFighterWin) {
    bodyElement = `Победил левый боец под ником ${name}`
  } else if (fighter.rightFighterWin) {
    bodyElement = `Победил правый боец под ником ${name}`
  } else {
    bodyElement = `Победил игрок под ником ${name}`
  }

  const winner = {
    title: `Winner ${name}`,
    bodyElement: bodyElement,
    onClose: () => {
      window.location.reload()
    }
  }
  showModal(winner)

}
